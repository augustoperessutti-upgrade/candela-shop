import { Component, Input, OnInit } from '@angular/core';
//MODELOS
import { Iproducts } from 'src/app/models/Iglobals';
//SERVICIOS
import { DiscountService } from 'src/app/services/discount.service';
import { SaveFavsService } from 'src/app/services/save-favs.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  @Input() public productsElements: Iproducts[] = [];
  
  constructor(public favsService: SaveFavsService, public promotion: DiscountService, public addCart: CartService) {}

  ngOnInit(): void {
  }

}
