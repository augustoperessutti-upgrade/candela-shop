import { Component, OnInit } from '@angular/core';

import { Iproducts } from 'src/app/models/Iglobals';

import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-home-view',
  templateUrl: './home-view.component.html',
  styleUrls: ['./home-view.component.scss']
})
export class HomeViewComponent implements OnInit {

  public productsElements: Iproducts[] = [];

  constructor(private requestService: RequestService) { 
    this.getProducts()
  }

  public getProducts() {
    this.requestService.getRequest('products').subscribe((data: any) => {
      this.productsElements = data;
    })
  }

  ngOnInit(): void {
  }

}
