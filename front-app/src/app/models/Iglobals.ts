export interface Iproducts {
    id: string;
    name: string;
    description: string;
    price: string;
    stock: number;
    image: Iimg;
}
export interface Iimg {
    src: string;
    alt: string;
}
